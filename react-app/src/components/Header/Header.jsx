import React from 'react';
import './Header.css';
import { Link } from "react-router-dom";
import Cart from '../Cart/Cart';
import Heart from '../Heart/Heart';

function Header() {
  return (
  <header className="site-header">
    <div className="header">
      <Link to='/' className='link-main'>
      <div className="site-header__logo">
        <div className="site-header__logo-icon">
          <img src="./image/favicon.svg" alt="иконка главная" className="site-header__icon" />
        </div>
        <h1 className="site-header__name">
          <span className="site-header__name site-header__name_first-word">Мой</span>Маркет
        </h1>
      </div>
      </Link>
      <div className="site-header__icons">
        <div className="site-header__heart-block">
          <img src="./image/icons/heart-icon.png" alt="избранное" className="site-header__heart-icon"></img>
            <Heart product={{id:2, name:"Iphone13"}}/>
        </div>
        <div className="site-header__basket-block">
          <img src="./image/icons/basket-icon.png" alt="корзина" className="site-header__basket-icon"></img>
            <Cart product={{id:1, name:"IPhone"}}/>
        </div>
      </div>
    </div>
  </header>
  )
}

export default Header;