import React from 'react';
import './Footer.css';
import { useCurrentDate } from "@kundinos/react-hooks";


function Footer () {

    const currentDate = useCurrentDate();
    const fullYear = currentDate.getFullYear();

  return (
<footer className="footer">
<div className="footer__block">
    <div className="footer__info">
        <div className="footer__name">
            © ООО "
            <div className="footer__first-word">Мой</div>
            {`Маркет", 2018-${fullYear}`}
        </div>
        <div>
            Для уточнения информации звоните по номеру
            <a href="tel:+7 900 000 0000" className="link link_direct">+7 900 000 0000</a>,
        </div>
        <div>
            а предложения по сотрудничеству отправляйте на почту
            <a href="mailto:partner@mymarket.com" className="link link_direct">partner@mymarket.com</a>
        </div>
    </div>
    <div>
        <a href="#" className="link link_direct">Наверх</a>
    </div>
</div>
</footer>
  )
}

export default Footer;