import './ButtonHeart.css';
import { addProduct, removeProduct } from '../reducers/heart-reducer';
import { useSelector, useDispatch } from 'react-redux';



function HeartProduct(props) {
  const { product } = props;
  const products = useSelector((store)=>store.favorites.products);
  const dispatch = useDispatch();
  
  
  const hasInHeart = products.some((prevProduct) => {
    return prevProduct.id === product.id;
  });

  function handleAddProduct(e, product) {
    dispatch(addProduct(product));
  }

  function handleRemove(e, product) {
    dispatch(removeProduct(product));
  }

  return (
    <>
      {hasInHeart ? (
        <button  
          className='btn_heart-remove'
          onClick={(e) => handleRemove(e, product)}>
        </button>
          ) : (
        <button 
          className='btn_heart' 
          onClick={(e) => handleAddProduct(e, product)}>
        </button>)
} </>
  )
}

export default HeartProduct;