import { createSlice } from "@reduxjs/toolkit";

export const cartSlice = createSlice ({
  name: "cart",

  initialState: {
    products: JSON.parse(localStorage.getItem("cart-products")) || []
  },

  reducers: {
    addProduct: function(prevState, action) {
      const product = action.payload;
      
      const newState = {
        ...prevState,
        products: [...prevState.products, product],
      };

      const data = JSON.stringify(newState.products)
      localStorage.setItem("cart-products", data);

      return newState;
    },
    removeProduct: (prevState, action) => {
      const product = action.payload;

      const removeState = {
        ...prevState,
        products: prevState.products.filter((prevProduct) => {
          return prevProduct.id !== product.id;
        })
      }

      localStorage.removeItem("cart-products")
      return removeState;
    }
  },
});

export const { addProduct, removeProduct } = cartSlice.actions;

export default cartSlice.reducer;