import { createSlice } from "@reduxjs/toolkit";

export const heartSlice = createSlice ({
  name: "heart",

  initialState: {
    products: JSON.parse(localStorage.getItem("heart-products")) || []
  },

  reducers: {
    addProduct: function(prevState, action) {
      const product = action.payload;
      
      const newState = {
        ...prevState,
        products: [...prevState.products, product],
      };

      const data = JSON.stringify(newState.products)
      localStorage.setItem("heart-products", data);

      return newState;
    },
    removeProduct: (prevState, action) => {
      const product = action.payload;

      const removeState = {
        ...prevState,
        products: prevState.products.filter((prevProduct) => {
          return prevProduct.id !== product.id;
        })
      }

      localStorage.removeItem("heart-products")
      return removeState;
    }
  },
});

export const { addProduct, removeProduct } = heartSlice.actions;

export default heartSlice.reducer;

