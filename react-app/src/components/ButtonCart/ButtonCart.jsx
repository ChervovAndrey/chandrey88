import './ButtonCart.css';
import { addProduct, removeProduct } from '../reducers/cart-reducer';
import { useSelector, useDispatch } from 'react-redux';



function CartProduct(props) {
  const { product } = props;
  const products = useSelector((store)=>store.cart.products);
  const dispatch = useDispatch();
  
  
  const hasInCart = products.some((prevProduct) => {
    return prevProduct.id === product.id;
  });

  function handleAddProduct(e, product) {
    dispatch(addProduct(product));
  }

  function handleRemove(e, product) {
    dispatch(removeProduct(product));
  }

  return (
    <>
      {hasInCart ? (
        <button  
          className='btn_remove'
          onClick={(e) => handleRemove(e, product)}>Товар уже в корзине
        </button>
          ) : (
        <button 
          className='btn btn_direct' 
          onClick={(e) => handleAddProduct(e, product)}>Добавить в корзину
        </button>)
} </>
  )
}

export default CartProduct;