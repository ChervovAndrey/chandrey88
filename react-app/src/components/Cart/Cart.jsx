import { useSelector } from 'react-redux';
import './Cart.css';



function Cart() {
  const products = useSelector((store)=>store.cart.products);
  const count = products.length;

  if (count) {
    return (
      <div className="basket__counter">{`${count}`}</div>
    )
  }

}

export default Cart;