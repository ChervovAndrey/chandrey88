const configs = [
  { 
    id: 1, 
    memory: "128 ГБ", 
  }, 
  { 
    id: 2, 
    memory: "256 ГБ", 
  }, 
  { 
    id: 3, 
    memory: "512 ГБ", 
  },
];

export default configs;