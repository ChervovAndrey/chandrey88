import React, {useState} from 'react';
import './ConfigButtons.css';
import configs from './configs';


function ConfigButtons() {
  const [activedColor, setActivedColor] = useState(0);

  function handleClick(e, id) {
    setActivedColor(id);
  }

  return (
    <div className="memory-info">
      <div>
        <h3 className="memory-info__selection">Конфигурация памяти: 128 ГБ</h3>
      </div>
      <div className="memory-info__button">
        {configs.map((config) => {
          const actived = config.id === activedColor;
          return (
          <button className={`memory-info__btn ${actived ? 'memory-info__btn_selected' : ''}`}
            key={config.memory}  
            onClick={(e) => handleClick(e, config.id)}>
            {config.memory}
          </button>
          );
        })}    
      </div>
    </div>
  )
}

export default ConfigButtons;