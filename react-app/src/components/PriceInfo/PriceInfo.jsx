import React from 'react';
import './PriceInfo.css';
import CartProduct from '../ButtonCart/ButtonCart';
import HeartProduct from '../ButtonHeart/ButtonHeart';

function PriceInfo() {
  return (
  <div className="price__info">
    <div className="price__name">
      <div className="price__phone">
        <div className="price__old">
          <span className="price__old-price"><s>75 990₽</s></span>
          <div className="price__discount">-8%</div>
          <HeartProduct product={{id:2, name:"Iphone13"}}/>
        </div>
        <div className="price__new">
          <b>67 990₽</b>
        </div>
        <div className="delivery">
          <div className="delivery__data">
            <div className="delivery__pickup">
              Самовывоз в четверг, 1 сентября — <b>бесплатно</b>
            </div>
            <div className="delivery__courier">
              Курьером в четверг, 1 сентября — <b>бесплатно</b>
            </div>
          </div>
        </div>
      </div>
        <CartProduct product={{id:1, name:"IPhone"}}/>                        
    </div>
  </div>
  )
}

export default PriceInfo;