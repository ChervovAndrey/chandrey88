import React, { Fragment } from 'react';
import './Reviews.css';
import reviews from './ReviewsList';

function Reviews() {
  return (
    <div className="feedback">
        {reviews.map((review, index) => {
            const separator = index === 0;

            return (
            <Fragment key={review.name}>
                <div  className="feedback__module">
                    <div className="feedback__main">
                        <img src={review.imageClient} alt={review.imageClient} className="feedback__photo-client"/>
                            <div className="feedback__two">
                                <h4 className="feedback__name-two">{review.name}</h4>
                                <img src={review.rating} alt={review.rating} className="feedback__rating-two"/>
                            </div>
                        </div>
                    <div className="feedback__block">
                        <h4 className="feedback__name">{review.name}</h4>
                        <img src={review.rating} alt={review.rating} className="feedback__rating"/>
                        <div className="feedback__text">
                                <div className="feedback__info-first">
                                <b>{review.experience}</b> {review.experienceText}
                            </div>
                            <div className="feedback__info-second"><b>{review.dignities}</b></div>
                            <div className="feedback__info-third">
                            {review.dignitiesText}
                            </div>
                            <div className="feedback__info-fourth"><b>{review.disadvantages}</b></div>
                            <div className="feedback__info-fifth">
                            {review.disadvantagesText}
                            </div>
                        </div>
                    </div>      
                </div>
                    <div className="line">
                        <div className={`${separator ? 'line__block' : ''}`}></div>
                    </div>
            </Fragment>
                
            );
        })}
    </div>
  )
}

export default Reviews;