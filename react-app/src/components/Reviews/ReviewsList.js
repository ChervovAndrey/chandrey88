const reviews = [
    {
        imageClient: './image/review/foto-client.png',
        name: "Марк Г.",
        rating: './image/review/rating.svg',
        experience: "Опыт использования:",
        experienceText: "менее месяца",
        dignities: "Достоинства:",
        dignitiesText: "это мой первый айфон после огромного количества телефонов на андроиде. всё плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая, ширик тоже на высоте.",
        disadvantages: "Недостатки:",
        disadvantagesText: "к самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь, а если нужно переносить фото с компа, то это только через itunes, который урезает качество фотографий исходное",
    },
    {
        imageClient: './image/review/foto-second client.png',
        name: "Валерий Коваленко",
        rating: './image/review/rating-second.svg',
        experience: "Опыт использования:",
        experienceText: "менее месяца",
        dignities: "Достоинства:",
        dignitiesText: "OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго",
        disadvantages: "Недостатки:",
        disadvantagesText: "Плохая ремонтопригодность",
    },
]
    
export default reviews; 
            
            
            