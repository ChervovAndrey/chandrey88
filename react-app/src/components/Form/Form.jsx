import React, { useState } from "react";
import "./Form.css";

let errorInitial = { name: null, rating: null };

function Form() {
    let [input, setInput] = useState(JSON.parse(localStorage.getItem('reviewFormInput')));
    let [error, setError] = useState({ errorInitial });
    localStorage.setItem('reviewFormInput', JSON.stringify(input));

    let handleSubmit = (event) => {
      event.preventDefault();

      if (input?.name.trim() === '') {
          setError({ ...error, name: 'Вы забыли указать имя и фамилию' });
          return;
      }

      let nameLength = input?.name.trim().length;
      
      if (nameLength <= 2) {
          setError({ ...error, name: 'Имя не может быть короче 2-х символов' });
          return;
      }

      let rating = +input?.rating;
      
      if (isNaN(rating) || rating < 1 || rating > 5) {
          setError({ ...error, rating: 'Оценка должна быть от 1 до 5' });
          return;
      }

      setInput({ name: "", rating: "", text: "" });
      alert('Вы успешно отправили отзыв')
      setError("");

    };
 
    let handleInputName = (event) => {
      setInput({ ...input, name: event.target.value });
    };

    let handleInputRating = (event) => {
      setInput({ ...input, rating: event.target.value });
    };

    let handleInputText = (event) => {
      setInput({ ...input, text: event.target.value });
    };
  
    return (
    <form onSubmit={handleSubmit}>
  <fieldset className="form">
      <legend className="form__head">Добавить свой отзыв</legend>
    <div className="form__container">
      <div className="form__first">
          <div className="form__container-first">
            <div className="form__name-input">
              <input className="form__name"
                type="text" 
                name="name" 
                value={input?.name}
                onChange={handleInputName}
                placeholder="Имя и фимилия"
              />
              <div className="error__name">
                <div className={`error__text ${error.name ? '' : 'hidden'}`}>{error.name}</div>
              </div>
            </div> 
            <div className="form__estimation-input">
              <input className="form__estimation" 
                type="number" 
                name="rating" 
                value={input?.rating}
                onInput={handleInputRating}
                placeholder="Оценка"
              />
              <div className="error__block">
                <div className={`error__count ${error.rating ? '' : 'hidden'}`}>{error.rating}</div>
              </div>
            </div>
          </div>
      </div>
      <div className="form__second">
        <div className="form__container-second">
          <textarea className="form__text" 
            name="text" 
            value={input?.text}
            onInput={handleInputText}
            placeholder="Текст отзыва">
          </textarea>
        </div>
        <div className="form__button-review">
          <button type='submit' className="form__button">Отправить отзыв</button>
        </div>
      </div>
    </div>
  </fieldset>
  </form>
  );
}

export default Form;