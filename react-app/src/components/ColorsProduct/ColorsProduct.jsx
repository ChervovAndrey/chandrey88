import React, {useState} from 'react';
import './ColorsProduct.css';
import colors from './colors';


function ColorsProduct() {
  const [activedColor, setActivedColor] = useState(0);

  function handleClick(e, id) {
    setActivedColor(id);
  }

  return (
    <div className="characteristics__color">
      {colors.map((color) => {
        const actived = color.id === activedColor;
        return (
        <button className={`characteristics__btn characteristics__btn_direct ${actived ? 'characteristics__btn_direct_selected' : ''}`}
        key={color.image}
        onClick={(e) => handleClick(e, color.id)}>
          <img alt={color.alt} src={color.image} />
        </button>
        );
      })}    
    </div>
  )
}

export default ColorsProduct;