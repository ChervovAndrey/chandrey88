import React from 'react';
import './PageProduct.css';
import ColorsProduct from "../ColorsProduct/ColorsProduct";
import ConfigButtons from '../ConfigMemory/ConfigButtons';
import Reviews from '../Reviews/Reviews';
import Form from '../Form/Form';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import PriceInfo from '../PriceInfo/PriceInfo';

function PageProduct() {
  return (
<div className="page">
    <Header/>
    <nav className="navigation">
        <div className="navigation__list">
            <a href="#" className="navigation__list-name navigation__list-name_direct navigation__list-name_bracket">Электроника</a>
            <a href="#" className="navigation__list-name navigation__list-name_direct navigation__list-name_bracket">Смартфоны и гаджеты</a>
            <a href="#" className="navigation__list-name navigation__list-name_direct navigation__list-name_bracket">Мобильные телефоны</a>
            <a href="#" className="navigation__list-name navigation__list-name_direct">Apple</a>
        </div>
    </nav>
    <main className="product">
        <div className="product__head">
            <h2 className="product__head-tittle">Смартфон Apple iPhone 13, синий</h2>
        </div>
        <div className="product__photo">
            <img src="./image/image-1.png" alt="Главная часть" className="product__image"/>
            <img src="./image/image-2.png" alt="Лицевая сторона" className="product__image"/>
            <img src="./image/image-3.png" alt="Изображение под углом" className="product__image"/>
            <img src="./image/image-4.png" alt="Камера" className="product__image"/>
            <img src="./image/image-5.png" alt="задняя и лицевая часть" className="product__image"/>
        </div>
        <section className="characteristics">
            <div className="characteristics__total">
                <div className="characteristics__info">
                    <h3 className="characteristics__name">Цвет товара: Синий</h3>
                        <ColorsProduct />
                        <ConfigButtons />
                    <section className="property">
                        <h3 className="property__name">Характеристики товара</h3>
                        <ul className="property__list">
                            <li className="property__title">Экран: <b>6.1</b></li>
                            <li className="property__title">
                                Встроенная память: <b>128 ГБ</b>
                            </li>
                            <li className="property__title">
                                Операционная система: <b>iOS 15</b>
                            </li>
                            <li className="property__title">
                                Беспроводные интерфейсы: <b>NFC, Bluetooth, Wi-Fi</b>
                            </li>
                            <li className="property__title">
                                Процессор:
                                <a href="https://ru.wikipedia.org/wiki/Apple_A15" target="_blank"
                                    className="link link_direct"><b>Apple A15 Bionic</b></a>
                            </li>
                            <li className="property__title">Вес: <b>173 г</b></li>
                        </ul>
                    </section>
                    <section>
                        <div className="description">
                            <h3 className="description__name">Описание</h3>
                            <div className="description__text">
                                <div className="description__paragraph">
                                    <br />Наша самая совершенная система двух камер.
                                    <br />Особый взгляд на прочность дисплея. <br />Чип, с
                                    которым всё супербыстро. <br />Аккумулятор держится заметно
                                    дольше. <br /><i>iPhone 13 - сильный мира всего.</i>
                                </div>
                                <div className="description__paragraph">
                                    Мы разработали совершенно новую схему расположения и
                                    развернули объективы на 45 градусов. Благодаря этому внутри
                                    корпуса поместилась наша лучшая система двух камер с
                                    увеличенной матрицей широкоугольной камеры. Кроме того, мы
                                    освободили место для системы оптической стабилизации
                                    изображения сдвигом матрицы. И повысили скорость работы
                                    матрицы на сверхширокоугольной камере.
                                </div>
                                <div className="description__paragraph">
                                    Новая сверхширокоугольная камера видит больше деталей в
                                    тёмных областях снимков. Новая широкоугольная камера
                                    улавливает на 47% больше света для более качественных
                                    фотографий и видео. Новая оптическая стабилизация со сдвигом
                                    матрицы обеспечит чёткие кадры даже в неустойчивом
                                    положении.
                                </div>
                                <div className="description__paragraph">
                                    Режим «Киноэффект» автоматически добавляет великолепные
                                    эффекты перемещения фокуса и изменения резкости. Просто
                                    начните запись видео. Режим «Киноэффект» будет удерживать
                                    фокус на объекте съёмки, создавая красивый эффект размытия
                                    вокруг него. Режим «Киноэффект» распознаёт, когда нужно
                                    перевести фокус на другого человека или объект, который
                                    появился в кадре. Теперь ваши видео будут смотреться как
                                    настоящее кино.
                                </div>
                            </div>
                        </div>
                    </section>
                    <div className="table">
                        <h3 className="table__topic">Сравнение моделей</h3>
                        <table className="table__block">
                            <thead className="table__head">
                                <tr className="table__main-line">
                                    <th className="table__name">Модель</th>
                                    <th className="table__name">Вес</th>
                                    <th className="table__name">Высота</th>
                                    <th className="table__name">Ширина</th>
                                    <th className="table__name">Толщина</th>
                                    <th className="table__name">Чип</th>
                                    <th className="table__name">Объём памяти</th>
                                    <th className="table__name">Аккумулятор</th>
                                </tr>
                            </thead>

                            <tbody className="table__body">
                                <tr className="table__line table__line_direct">
                                    <td className="table__meaning">iPhone 11</td>
                                    <td className="table__meaning">194 грамма</td>
                                    <td className="table__meaning">150.9 мм</td>
                                    <td className="table__meaning">75.7 мм</td>
                                    <td className="table__meaning">8.3 мм</td>
                                    <td className="table__meaning">A13 Bionic chip</td>
                                    <td className="table__meaning">до 128 Гб</td>
                                    <td className="table__meaning">до 17 часов</td>
                                </tr>
                                <tr className="table__line table__line_direct">
                                    <td className="table__meaning">iPhone 12</td>
                                    <td className="table__meaning">164 грамма</td>
                                    <td className="table__meaning">146.7 мм</td>
                                    <td className="table__meaning">71.5 мм</td>
                                    <td className="table__meaning">7.4 мм</td>
                                    <td className="table__meaning">A14 Bionic chip</td>
                                    <td className="table__meaning">до 256 Гб</td>
                                    <td className="table__meaning">до 19 часов</td>
                                </tr>
                                <tr className="table__line table__line_direct">
                                    <td className="table__meaning">iPhone 13</td>
                                    <td className="table__meaning">174 грамма</td>
                                    <td className="table__meaning">146.7 мм</td>
                                    <td className="table__meaning">71.5 мм</td>
                                    <td className="table__meaning">7.65 мм</td>
                                    <td className="table__meaning">A15 Bionic chip</td>
                                    <td className="table__meaning">до 512 Гб</td>
                                    <td className="table__meaning">до 19 часов</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="price__block">
                    <PriceInfo />
                    <div className="commerce">
                        <div className="commerce__text">Реклама</div>
                        <div className="commerce__list">
                            <iframe className="commerce__advertisement" src="./ads.html"></iframe>
                            <iframe className="commerce__advertisement" src="./ads.html"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div className="reviews">
            <div className="reviews__block">
                <div className="reviews__line">
                    <h3 className="reviews__header">Отзывы</h3>
                    <span className="reviews__count">425</span>
                </div>
            </div>
                <Reviews />
            <Form />
        </div>
    </main>
    <Footer/>
</div>
  );
}

export default PageProduct;