import { useSelector } from 'react-redux';
import './Heart.css';



function Heart() {
  const products = useSelector((store)=>store.favorites.products);
  const count = products.length;

  if (count) {
    return (
      <div className="heart__counter">{`${count}`}</div>
    )
  }
  
}

export default Heart;

