import React from 'react';
import './PageIndex.css';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import { Link } from "react-router-dom";

function PageIndex() {
  return (
    <div className='page-index'>
      <Header/>
        <div className='page-text'>
          Здесь должно быть содержимое главной страницы. 
          Но в рамках курса главная страница используется лишь в демонстрационных целях
          <Link to='/product' className='page-link'>Перейти на страницу товара</Link>
        </div>
      <Footer/>
    </div>
  )
}

export default PageIndex;