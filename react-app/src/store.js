import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "./components/reducers/cart-reducer";
import heartReducer from "./components/reducers/heart-reducer"

let count = [];
const logger = (store) => (next) => (action) => {
  count.push(action);
 
  let result = next(action);
 
  console.log(`Количество обработанных действий:`, count.length)
  return result;
};

export const store = configureStore ({ 
  reducer:{
    cart: cartReducer,
    favorites: heartReducer,
  },

  middleware: [logger],
});