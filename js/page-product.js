"use strict";

function formBlock() {
  let formName = document.querySelector('.form__name');
  let formEstimation = document.querySelector('.form__estimation');
  let submit = document.querySelector('.form__button');
  let formText = document.querySelector('.form__text');

  function sendReview(event) {
    event.preventDefault();

    let name = formName.value;
    let score = formEstimation.value;

    if (name.length == 0) {
      formName.style.background = "yellow";
      setTimeout (() => {
        formName.style.background="";
      },3000);
      alert("Вы забыли указать имя и фамилию");
    } else if (name.length < 2) {
      formName.style.background = "green";
      setTimeout (() => {
        formName.style.background="";
      },3000);
      alert("Имя не может быть короче 2-х символов");
    } else {
      console.log(name)
    }

    if (score.length == 0) {
      formEstimation.style.background = "blue";
      setTimeout (() => {
        formEstimation.style.background="";
      },3000);
      alert("Оценка должна быть от 1 до 5");
    } else if (!score) {
      formEstimation.style.background = "orange";
      setTimeout (() => {
        formEstimation.style.background="";
      },3000);
      alert("Оценка должна быть от 1 до 5");
    } else if (score < 1 || score > 5) {
      formEstimation.style.background = "red";
      setTimeout (() => {
        formEstimation.style.background="";
      },3000);
      alert("Оценка должна быть от 1 до 5");
    } else {
      console.log(score)
    }

    localStorage.removeItem("name");
    localStorage.removeItem("estimation");
    localStorage.removeItem("text");

  }

  function saveData(event) {
    let value = event.target.value;
    let name = event.target.getAttribute("name");
    let estimation = event.target.getAttribute("estimation");
    let text = event.target.getAttribute("text");
    
    localStorage.setItem(text, value);
    localStorage.setItem(estimation, value);
    localStorage.setItem(name, value);
  }

  formText.value = localStorage.getItem('text')
  formEstimation.value = localStorage.getItem('estimation')
  formName.value = localStorage.getItem('name')

  formText.addEventListener('input', saveData);
  formEstimation.addEventListener('input', saveData);
  formName.addEventListener('input', saveData);
  submit.addEventListener('click', sendReview);
  
}

formBlock()


function buttonBasket() {
  let basketCounter = document.querySelector('.basket__counter');
  let btn = document.querySelector('.btn');

  basketCounter.classList.remove("basket__counter");

  function addedToCart(event) {
    event.preventDefault();
    let isBuy = !basketCounter.classList.contains("basket__counter")
    
    if (isBuy) {
      basketCounter.classList.add("basket__counter");
      basketCounter.textContent = "1";
      btn.style.background = 'gray';
      btn.innerHTML = "Товар уже в корзине";
      localStorage.setItem('btn', isBuy);
    } else {
      basketCounter.classList.remove("basket__counter");
      basketCounter.textContent = "";
      btn.style.background = '#f36223';
      btn.innerHTML = "Добавить в корзину";
      localStorage.removeItem('btn')
    }
  }

  if (localStorage.getItem('btn')) {
    basketCounter.classList.add("basket__counter");
    basketCounter.textContent = "1";
    btn.style.background = 'gray';
    btn.innerHTML = "Товар уже в корзине";
  };
  
  btn.addEventListener('click', (e) => addedToCart(e));  
  
}


buttonBasket()