"use strict";

// Упражнение 1
let a = 2;
while (a < 21) {
  console.log(a);
  a = a + 2;
}

// Упражнение 2
let sum = 0;
let count = 0;
while (count !== 3) {
  let value = +prompt("Введите число", "");

  if (!value) {alert("Ошибка, выввели не число"); break;}
  sum += value;
  count++;
}
alert("Сумма " + sum);

// Упражнение 3
function getNameOfMonth(n) {
  if (n === 0) return 'Январь';
  if (n === 1) return 'Февраль';
  if (n === 2) return 'Март';
  if (n === 3) return 'Апрель';
  if (n === 4) return 'Май';
  if (n === 5) return 'Июнь';
  if (n === 6) return 'Июль';
  if (n === 7) return 'Август';
  if (n === 8) return 'Сентябрь';
  if (n === 9) return 'Октябрь';
  if (n === 10) return 'Ноябрь';
  if (n === 11) return 'Декабрь';
}

for (let n = 0; n < 12; n++) {
  const month = getNameOfMonth(n);

  if (month === 'Октябрь') continue;

  console.log(month);
}


// Упражнение 4

//;(function () {
  // IIFE — это функция, которая выполняется сразу же после того, как была определена. При помощи IIFE мы можем использовать одинаковые названия переменных, не боясь, что они случайно перезапишут значения переменных из чужих модулей.
//})()
//Пример:
// ;(function module1() {
//  const a = 42
//  console.log(a)
// })()
//;(function module2() {
//  const a = '43!'
//  alert(a)
// })()