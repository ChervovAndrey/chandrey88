"use strict";

//Упражнение 1
let user = {};
/**
 * 
 * @param {*} obj //данный параметр создан для определения объекта, с помощью цикла for..in перебираются все свойства данного объекта
 * @returns // эта функция возращает в консоль true если объект не имеет свойств, при их наличии возвращается false
 */
function isEmpty(obj) {
  
  for (let key in obj) {
  return false;
  };

  return true;
};

let check = isEmpty(user);

console.log(check);

//Упражнение 2

// В отдельном файле data.js

//Упражнение 3
let salaries = {
  
  John: 100000,
  
  Ann: 160000,
  
  Pete: 130000,
};

function raiseSalary(percent) {
  /**
   * Проверил входные данные
   * Написал формулу умножения зарплаты на %, с округление в меньшую сторону до целого числа. Ввод процентов, на которые нужно поднять зарплату, осуществляется через +promt, написана формула для перевода данного числа в проценты.
   * Выведена общая сумма после изменения процентов.
   */
   let sum = 0;
   let a = +prompt("Повышение зарплаты в %:", "");

   for (let key in salaries) {
    sum += (Math.floor(salaries[key] * (1 + a / 100)));
   }

   return sum;
}

let sumTotal = raiseSalary(salaries);

console.log(sumTotal);

