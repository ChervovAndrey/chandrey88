"use strict";

class AddReviewForm {
  constructor(form) {
    this.form = form;
    this.inputs = form.querySelectorAll('.input');
  };

  sendReview(event) {
    event.preventDefault();

    let name = formName.value;
    let score = formEstimation.value;

    if (name.length == 0) {
      formName.style.background = "yellow";
      setTimeout (() => {
        formName.style.background="";
      },3000);
      alert("Вы забыли указать имя и фамилию");
    } else if (name.length < 2) {
      formName.style.background = "green";
      setTimeout (() => {
        formName.style.background="";
      },3000);
      alert("Имя не может быть короче 2-х символов");
    } else {
      console.log(name)
    }

    if (score.length == 0) {
      formEstimation.style.background = "blue";
      setTimeout (() => {
        formEstimation.style.background="";
      },3000);
      alert("Оценка должна быть от 1 до 5");
    } else if (!score) {
      formEstimation.style.background = "orange";
      setTimeout (() => {
        formEstimation.style.background="";
      },3000);
      alert("Оценка должна быть от 1 до 5");
    } else if (score < 1 || score > 5) {
      formEstimation.style.background = "red";
      setTimeout (() => {
        formEstimation.style.background="";
      },3000);
      alert("Оценка должна быть от 1 до 5");
    } else {
      console.log(score)
    }

    localStorage.removeItem("name");

  }

  saveData(inputNode) {
    let value = inputNode.value;
    let name = inputNode.getAttribute("name");
    
    localStorage.setItem(name, value);
  }

  setData() {
    this.inputs.value = localStorage.getItem('name')
  }

  bindItems() {
    this.form.addEventListener('submit', this.sendReview);

    this.inputs.forEach((item) => {
      item.addEventListener('input', () => this.saveData(item))
    });
    
  }
}

let add_formName = new AddReviewForm(document.querySelector(".form"));
add_formName.setData()
add_formName.bindItems()