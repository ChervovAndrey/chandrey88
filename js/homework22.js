"use strict";

//Упражнение 1
let numbers = [1, 'junior', 7, '3', 5];

function getSum(arr) {
  let sum = 0;
  
  for (let a = 0; a < arr.length; a++) {
    if (+arr[a]) {
      sum += +arr[a]
    }
  };

  return sum;
};

let arr1 = getSum(numbers);

console.log(arr1);

//Упражнение 2

// В отдельном файле data.js

//Упражнение 3

// В корзине один товар
let cart= [4884];

//Создать функцию для добавления товара в корзину
function addToCart(id) {
  let cartSet = new Set(cart);
  cartSet.add(id);
  cart = Array.from(cartSet);
};

//Создать функцию для удаления товара из корзины
function removeFromCart(id) {
  let cartSet = new Set(cart);
  cartSet.delete(id);
  cart = Array.from(cartSet);
};

removeFromCart(4884);
addToCart(3456);
console.log(cart);


