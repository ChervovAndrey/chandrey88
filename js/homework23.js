"use strict";

//Упражнение 1
let count = +prompt("Введите число", "");

let intervalId = setInterval(()=> {
  count=count-1;

  let value = `Осталось ${count}`;
  
  console.log(value);
  
// Останавливаем интервал, когда дошли до нуля
  if(count===0) {
    clearInterval (intervalId);
    let end = `Время вышло!`;
    console.log(end)
  }

},1000);

//Написать код если ввели не число и останавливать интервал
if(!count) {
  console.log("Ошибка, вы ввели не число!"); 
  clearInterval (intervalId);
}

//Упражнение 2
let intervalTime = setInterval(()=> {
  let promise = fetch ("https://reqres.in/api/users");
  promise

  .then(function (response) {
    return response.json();
  })

  //— Yury Kundin (yury.kundin@yandex.ru)

  .then(function (response) {
    console.log(`Получили пользователей: ${response.per_page}`);
    console.log(`- ${response.data[0].first_name} ${response.data[0].last_name} (${response.data[0].email})`);
    console.log(`- ${response.data[1].first_name} ${response.data[1].last_name} (${response.data[1].email})`);
    console.log(`- ${response.data[2].first_name} ${response.data[2].last_name} (${response.data[2].email})`);
    console.log(`- ${response.data[3].first_name} ${response.data[3].last_name} (${response.data[3].email})`);
    console.log(`- ${response.data[4].first_name} ${response.data[4].last_name} (${response.data[4].email})`);
    console.log(`- ${response.data[5].first_name} ${response.data[5].last_name} (${response.data[5].email})`);
  })

  .catch(function () {
    console.log("Кажется бэкенд сломался :(");
  });

  clearInterval (intervalTime);
},);

console.log(`Время выполнения функции: ${intervalTime}mc`);
